# README #

Dynamic miss handling and replacement policies into gem5: The goal of this repository is to analyze the performance of different replacement/miss handling policies using the gem5 simulator. The [simulator](http://www.gem5.org/Main_Page) only has basic policies for this. Researchers propose new ideas to improve these policies but other researchers cannot analyze their proposed ideas since the code is not opensourced. This repository is to replicate their algorithms into something runnable by gem5 users and researchers. 

### Use Cases: ###

* Researchers interested in analyzing performance of different replacement policies.
* Researchers interested in analyzing performance of different miss handling policies.
* We will continue to add different replacement policies that have been presented in multiple research policies in the past decade. 

### How to build: ###

* Copy the following files in this repository into gem5 version 4.3 (has not been tested on other versions)
* * Replacement policy files should be placed in src/mem/cache/tags
* * Miss handling policy should be placed into src/mem/cache
* This will replace the files previously located in those directories. 
* To analyze the performance of different replacement policies, there are two options:
* * Replace the original lru.cc in the tags directory with lru-clru.cc
* * Edit the sconscript file located in src/mem/cache to add the lru-clru.cc file.
* * Build simply with: scons build/gem5.opt -j4


### How to run: ###
* Sample run, Note: The difference in policies will only be significant when the simulator is run with many instructions. 

* * ./build/ALPHA/gem5.opt configs/example/se_spec2006.py --mem-type=SimpleMemory --cpu-type=detailed --bench=zeusmp --caches --l1d_size=64kB --l1i_size=64kB --l1d_assoc=4 --l1i_assoc=4 --l2cache --l2_size=512kB --l2_assoc=16 --l1i_hit_latency=3 --l1d_hit_latency=3 --l2_hit_latency=24 --cacheline_size=64 --fetchBufferSize=64 --mem-size=8192MB --cpu-clock=4GHz --dispatchWidth=16 --issueWidth=4 --LQEntries=48 --SQEntries=48 --numROBEntries=128 --numPhysIntRegs=256 --numPhysFloatRegs=256 --cachePorts=6 --l1d_tgts_per_mshr=16 --l1i_tgts_per_mshr=32 --l2_tgts_per_mshr=32 --l1i_mshrs=8 --l1d_mshrs=16 --l2_mshrs=8 -n 1 --maxinsts=1000000  --fast-forward=100000000 --warmup-insts=10000000